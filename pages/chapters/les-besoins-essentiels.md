---
has_children: true
nav_order: 2
title: Les besoins essentiels d'une équipe
uuid: 6XxbE1eOjzCBwvIZjxJAw
---

# Les besoins essentiels d'une équipe

![Les 5 besoins de l'équipe](../../assets/uploads/les-5-besoins-de-lequipe.jpg)

## Organiser et animer des réunions

L’organisation de réunions à distance met en œuvre de nombreux logiciels libres comme **Jisti**, **Framadate**,**Etherpad** et **Scrumblr** :

- pour trouver une date commune, 
- pour réaliser une visioconférence
- pour prendre des notes pendant la réunion

[Lire comment démarrer >](../docs/organiser-des-reunions.html)

## Prendre des décisions à distance

Faire de la gouvernance partagée à distance présente des difficultés techniques. Le mouvement Occupy l’a bien compris en créant **Loomio** en 2013 pour prendre des décisions éclairées et égalitaires en ligne. Il s’agit d’un forum pourvu d’outils dédiés pour couvrir tous les cas d’arbitrage classiques, transposés au numérique.

[Lire comment démarrer >](../docs/prendre-des-decisions-a-distance.html)

## Partager des fichiers & des calendriers

Le partage de fichiers entre personnes passe par une solution où tous les documents sont centralisés sur un serveur central. Chaque membre de l’équipe accède aux fichiers d’où il veut et quand il veut pour travailler dessus. Les logiciels libres que nous avons sélectionné sont **NextCloud** et **OnlyOffice**.

Nextcloud offre en plus la possibilité de gérer ses agendas personnels et d’équipe.

[Lire comment démarrer >](../docs/partager-des-fichiers-et-des-calendriers.html)

## Discuter en équipe

Les solutions de “chat ” foisonnent, mais sont-elles toutes adaptées au travail en équipe ? **Mattermost** et **Rocket Chat** sont des logiciels libres de messagerie instantanée « orienté équipes ». Au lieu de simplement créer des discussions entre des personnes, ces logiciels fournissent un espace dédié à une organisation ou des équipes pour organiser les échanges.

[Lire comment démarrer >](../docs/discuter-en-equipe.html)

## Gérer les mots de passe de l’équipe

Aujourd’hui, il est devenu difficile de garder la trace de tous nos mots de passe et de se le transmettre de manière sécurisée. **Keepass** est un gestionnaire de mots de passe open-source, libre et gratuit, qui facilitera grandement la vie de vos équipes en stockant dans un seul fichier chiffré les mots de passe.

[Lire comment démarrer >](../docs/gerer-les-mots-de-passe-de-lequipe.html)