---
title: "Partager des fichiers & des calendriers"
parent: Les besoins essentiels d'une équipe
nav_order: 3
---

# Partager des fichiers & des calendriers

Le partage de fichiers entre personnes passe par une solution où tous les documents sont centralisés sur un serveur central. Chaque membre de l’équipe accède aux fichiers d’où il veut et quand il veut pour travailler dessus. Les logiciels libres que nous avons sélectionné sont NextCloud et OnlyOffice. Nextcloud offre en plus la possibilité de gérer ses agendas personnels et d’équipe.

[Partage de fichiers et des calendriers](../../assets/uploads/partage-de-fichiers-calendriers.jpg)

## Nextcloud et OnlyOffice

Nextcloud est un espace de travail collaboratif en ligne avec, parmi de nombreuses autres fonctionnalités, des fichiers partagés et des calendriers communs.

OnlyOffice est une suite bureautique pour éditer des fichiers de type traitement de texte, tableur et diaporama. Elle s’intègre notamment à NextCloud pour travailler à plusieurs en temps réel sur un même document et sans quitter son navigateur internet.

Alternative à OneDrive, Dropbox, Google Drive, Office365 et Google Docs.

## Où démarrer ?

| Hébergeur | Outils proposés | Période d'essai | Tarif |
|-----------|-----------------|-----------------|-------|
| [IndieHosters - Liiibre](https://indiehosters.net/) | Nextcloud/OnlyOffice | [15 jours](https://indiehosters.net/tester-liiibre/) | 8€ HT / mois / utilisateur actif |
| [Ethibox](https://ethibox.fr/nextcloud) | Nextcloud | 7 jours | 19€ HT / mois avec 50G d'espace de stockage |
| [Zaclys](https://www.zaclys.com/cloud-pro/) | Nextcloud/OnlyOffice | - | De 260€HT à 990€HT - Possibilité de création de compte personnel gratuit avec 2Go d'espace de stockage. |
| [OnlyOffice](https://www.onlyoffice.com/fr/) | OnlyOffice | 30 jours | [Gratuit pour organisme à but non lucratif](https://www.onlyoffice.com/fr/nonprofit-organizations.aspx) ou pour usage personnel. [A partir de 2,4€ HT / utilisateur / mois](https://www.onlyoffice.com/fr/for-small-business.aspx) pour les autres organisations |

Plus d'hébergeurs proposés sur [chatons.org](https://www.chatons.org/) :

- [Nextcloud OnlyOffice](https://www.chatons.org/search/by-service?service_type_target_id=337&field_alternatives_aux_services_target_id=All&field_software_target_id=365&field_is_shared_value=All&title=)
- [Nextcloud](https://www.chatons.org/search/by-service?service_type_target_id=146&field_alternatives_aux_services_target_id=All&field_software_target_id=271&field_is_shared_value=All&title=)