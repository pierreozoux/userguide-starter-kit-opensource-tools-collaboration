---
title: "Discuter en équipe"
nav_order: 1
parent: Les besoins essentiels d'une équipe
---

# Discuter en équipe

Les solutions de “chat ” foisonnent, mais sont-elles toutes adaptées au travail en équipe ? Mattermost et Rocket Chat sont des logiciels libres de messagerie instantanée « orienté équipes ». Au lieu de simplement créer des discussions entre des personnes, ces logiciels fournissent un espace dédié à une organisation ou des équipes pour organiser les échanges.

![Discuter en équipe](../../assets/uploads/discuter-en-equipe.jpg)

## Les outils libres pour organiser des réunions

- [Rocket Chat](https://www.rocket.chat/) est une messagerie instantanée pour les organisations. Elle regroupe de manière organisée toutes les discussions en un seul endroit. Alternative à [Slack](https://slack.com/intl/fr-fr/).
- [Mattermost](https://mattermost.com/) est une messagerie instantanée pour organiser les discussions en équipe. Elle regroupe de manière organisée toutes les discussions en un seul endroit. Alternative à [Slack](https://slack.com/intl/fr-fr/).

Il existe d'autres outils comme Zulip par exemple. Pour choisir l'outil adapté en fonction de ses particularités, vous pouvez participer à la discussion [Comment faire le choix entre Mattermost, Rocket.Chat ou Zulip?](https://kafe.koweb.fr/t/comment-faire-le-choix-entre-mattermost-rocket-chat-ou-zulip/254)

## Où démarrer ?

| Hébergeur | Outils proposés | Période d'essai | Tarif |
|-----------|-----------------|-----------------|-------|
| [IndieHosters - Liiibre](https://indiehosters.net/) | Rocket.Chat | [15 jours](https://indiehosters.net/tester-liiibre/) | 8€ HT / mois / utilisateur actif |
| Framasoft | [Mattermost](hhttps://framateam.org)  | - | En libre accès |
| [Colibris Outils Libres](https://www.colibris-outilslibres.org/) | [Mattermost](https://www.colibris-outilslibres.org/services/tchat-mattermost/) | - | En libre accès |
| [Ethibox](https://ethibox.fr/) | [Rocket.Chat](https://ethibox.fr/rocketchat), [Mattermost](https://ethibox.fr/mattermost) | 7 jours | 19€ HT / mois avec 50G d'espace de stockage |

Plus d'hébergeurs proposés sur [chatons.org](https://www.chatons.org/) :

- [Rocket.Chat](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=252&field_is_shared_value=All&title=)
- [Mattermost](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=250&field_is_shared_value=All&title=)
- [Zulip](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=612&field_is_shared_value=All&title=)